import React from 'react';

import {View} from 'react-native';

import theme from '../theme';

const separator = () => {
    return (
        <View
            style={{
                height: 1,
                backgroundColor: theme.SECONDARY_COLOR,
            }}
        />
    );
}

export default separator;