import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import theme from '../theme';


const InfoPanel = props => {
    return (
        <View style={[styles.infoPanel, { backgroundColor: props.color }]}>
            <FontAwesomeIcon style={styles.icon} icon={props.icon} color="white" size={props.size} />
            <Text style={styles.title}>{props.title}</Text>
        </View>
    );
};

InfoPanel.propTypes = {
    title: PropTypes.string.isRequired,
    icon: PropTypes.object.isRequired,
    size: PropTypes.number,
}

const styles = StyleSheet.create({
    infoPanel: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        height: 75,
        marginHorizontal: 5,
        borderRadius: 10,
    },
    icon: {
        margin: 5,
    },
    title: {
        color: 'white',
        fontSize: theme.FONT_SIZE_M,
        textAlign: 'center',
    },
})

export default InfoPanel;