import React from 'react';
import { View, StyleSheet, Text } from 'react-native';

import PropTypes from 'prop-types';
import theme from '../theme';

import moment from '../localization/moment';


const Calendar = props => {
    const calendar = () => {
        let months = [];
        let rows = [];
        for (let i = 1; i < 13; i++) {
            let added = false;
            for (let k = 0; k < props.months.length; k++) {
                if (props.months[k] == i) {
                    added = true;
                    months.push(<View key={i} style={[styles.icon]}><Text style={styles.text}>{moment().month(i - 1).format('MMM')}</Text></View>);
                    break;
                }
            }
            if (!added) {
                months.push(<View key={i} style={[styles.icon, { opacity: 0.3 }]}><Text style={styles.text}>{moment().month(i - 1).format('MMM')}</Text></View>)
            }

            if (i % 4 === 0) {
                rows.push(<View style={styles.calendar} key={i}>{months}</View>);
                months = [];
            }
        }
        return rows;
    };

    return (
        <View style={[styles.container, props.style]}>{calendar()}</View>
    );
};

Calendar.propTypes = {
    months: PropTypes.arrayOf(PropTypes.number).isRequired,
    style: PropTypes.object
}

const styles = StyleSheet.create({
    container: {
        // flex: 1,
    },
    calendar: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        height: 40,
    },
    icon: {
        flex: 1,
        marginHorizontal: 5,
        backgroundColor: theme.ORANGE_COLOR,
        borderRadius: 10,
        height: 30,
        justifyContent: 'center',
        alignItems: 'center'
    },
    text: {
        color: theme.WHITE_COLOR
    }
})

export default Calendar;