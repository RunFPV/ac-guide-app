import { createStore, combineReducers, applyMiddleware } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import thunk from 'redux-thunk';
import { apiMiddleware } from 'redux-api-middleware';
import api from 'redux-cached-api-middleware';
import AsyncStorage from '@react-native-community/async-storage';

import reducers from './reducers';


// Middleware: Redux Persist Config
const persistConfig = {
    key: 'root',
    storage: AsyncStorage,
    // Whitelist (Save Specific Reducers)
    whitelist: [],
    // Blacklist (Don't Save Specific Reducers)
    blacklist: [],
};

const persistedReducer = persistReducer(
    persistConfig,
    combineReducers({
        ...reducers,
        [api.constants.NAME]: api.reducer,
    })
);

const store = createStore(
    persistedReducer,
    applyMiddleware(
        thunk,
        apiMiddleware
    ),
);

let persistor = persistStore(store);

export {
    store,
    persistor,
};