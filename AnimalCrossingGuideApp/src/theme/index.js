const theme = {
    PRIMARY_COLOR: "#22b573",
    SECONDARY_COLOR: "#CED0CE",

    RED_COLOR: "#e74c3c",
    BLUE_COLOR: "#3498db",
    ORANGE_COLOR: "#d35400",
    YELLOW_COLOR: "#f1c40f",
    WHITE_COLOR: "white",
    BLACK_COLOR: "black",
    

    FONT_SIZE_L: 22,
    FONT_SIZE_M: 17,
}

export default theme;