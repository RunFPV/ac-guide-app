import React from 'react';
import { View, Image, StyleSheet, Text } from 'react-native';

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';

import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faHome } from '@fortawesome/free-solid-svg-icons';

import * as s from '../screens';
import theme from '../theme';
import logo from '../../assets/logo.png';

import i18n from '../localization/i18n';

const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();


const StackNavigator = () => {
    return (
        <Stack.Navigator screenOptions={{headerBackTitle: i18n.t('navigator.back'), headerTitleAlign:'center'}}>
            <Stack.Screen name="Home" component={s.HomeScreen} options={{ headerTitle: props => (<View style={styles.logoContainer}><Image resizeMode="cover" style={styles.logo} source={logo} /></View>)}}/>
            <Stack.Screen name="List" component={s.ListScreen} options={({ route }) => ({ title: (<FontAwesomeIcon color={theme.PRIMARY_COLOR} size={30}  icon={route.params.name}/>)})} />
            <Stack.Screen name="Detail" component={s.DetailScreen} options={({ route }) => ({ title: route.params.name})} />
        </Stack.Navigator>
    );
}

const AppNavigator = () => {
    return (
        <Tab.Navigator
            tabBarOptions={{
                showLabel: false,
                activeTintColor: theme.PRIMARY_COLOR
            }}
        >
            <Tab.Screen
                name="Home"
                component={StackNavigator}
                options={{
                    tabBarIcon: ({ color, size }) => (
                        <FontAwesomeIcon color={color} icon={faHome} size={size} />
                    ),
                }}
            />
        </Tab.Navigator>
    );
}

const Navigator = AppNavigator;

const styles = StyleSheet.create({
    logoContainer: {
        flex: 1,
        alignSelf: 'center',
        padding: 5,
    },
    logo: {
        flex: 1,
        alignSelf: 'center',
        resizeMode: 'contain'
    }
});

export default Navigator;