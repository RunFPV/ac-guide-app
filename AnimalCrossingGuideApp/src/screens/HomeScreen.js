import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import theme from '../theme';

import { faFish, faBug } from '@fortawesome/free-solid-svg-icons';
import SpringboardButton from '../components/SpringboardButton';

import i18n from '../localization/i18n';


const HomeScreen = props => {
    return (
        <View style={styles.container}>
            <Text style={styles.title}>Guide</Text>

            <View style={styles.row}>
                <SpringboardButton title={i18n.t('home.fishes')} icon={faFish} onPress={() => props.navigation.navigate('List', { name: faFish, category: 1 })} />
                <SpringboardButton title={i18n.t('home.bugs')} icon={faBug} onPress={() => props.navigation.navigate('List', { name: faBug, category: 2 })} />
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: theme.WHITE_COLOR
    },
    row: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    title: {
        textAlign: 'center',
        fontSize: theme.FONT_SIZE_L,
        color: theme.BLACK_COLOR,
        padding: 10,
    },
});

export default HomeScreen;
