import config from './config';
import api from 'redux-cached-api-middleware';


export const getResults = (state, cacheKey) => {
    return api.selectors.getResult(state, cacheKey)
}

export const clearCache = (cacheKey) => {
    return api.actions.clearCache(cacheKey);
}

export const getAnimals = (cacheKey, params) => {
    return api.actions.invoke({
        method: 'GET',
        headers: {
            Accept: 'application/json',
            Authorization: 'Api-Key ' + config.SECRET_KEY,
        },
        endpoint: config.URL + '/animals/' + buildQuery(params),
        cache: {
            key: cacheKey,
            strategy: api.cache
                .get(api.constants.CACHE_TYPES.SIMPLE_SUCCESS)
                .buildStrategy(),
        },
    })
};

export const buildQuery = (params) => {
    if (params) {
        return '?' + Object.keys(params).map((key) => {
            if (params[key]) {
                return encodeURIComponent(key) + '=' + encodeURIComponent(params[key])
            }
        }).join('&');
    }
};